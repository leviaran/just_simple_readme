# SCREEN

<br/>

### Jadwal
**Screen** jadwal bertugas untuk menampilkan hari hari yang bertepatan dengan hari berlangsungnya event. Berikut kriteria yang harus ada dalam jadwal, silakan untuk berkreasi dan berinovasi untuk menambahkan;

- Memilih hari untuk mendapatkan Event dari hari Senin Sampai Minggu
- Fitur Search dan filter
- Fitur tanggal berdasarkan hari atau keyword
- Untuk setiap card event harus menyediakan
    - Bulan
    - tanggal
    - judul
    - excerpt
    - share
    - foto
    - favorite
    - tanda gratis atau berbayar

<br/>

### Profile
**Screen** profile menampilakan data pengguna, data pengguna ini digunakan untuk mengidentifikasi perlengkapan aplikasi untuk kebutuhan pengguna, detail perlengkapan adalah sebagai berikut;

- Komplain
- Favorite
- Poin saya
- Ramein pay
    - kartu saya
    - Saldo saya
- Help Center/Bantuan
- Pengaturan
- FAQ
- Logout
- Edit Account
    - Photo user
    - Change password
    - Email
    - Phone number
    - dst;

<br/>

### Pengaturan
**Screen** pengaturan digunakan untuk mengatur perlengkapan yang mendukung pengguna menggunakan aplikasi, berikut beberapa pilihan pengaturan yang dapat digunakan;

- Currency
- Language
- Push Notification
- Newsletter
- Terms & condion/term of use
- Privary policy
- Pengaturan Pembayaran
    - Rekening pengguna
- Pemberitahuan
- Open Source
- Lisensi
- Rate us
- Versi Aplikasi

<br/>

### Payment/Pembayaran
**Screen** payment digunakan ketik pengguna sudah membeli dan checkout, pengguna akan diarahkan ke menu pembayaran/payment, berikut beberapa atribut yang ditampilkan pada screen;

- Jumlah harga total yang mau dibayarkan/detail tagihan
- Pilih metode pembayaran
    - Transfer bank
    - Saldo Ramein
    - Cicilan kartu kredit
    - kartu kredit
    - Gerai retail
    - Pembayaran instan



